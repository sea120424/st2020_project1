const chai = require("chai");
const expect = chai.expect; 
chai.use(require("chai-sorted"));
import { sorting, compare, average } from '../src/utils/helper';


test('Should sort the array in ascending order', () => {
	const arr = [5,4,3,2,1];
	const arrSorted = [1,2,3,4,5];
	const newArr = sorting(arr);
	expect(newArr).to.eql(arrSorted);
})

test('Should sort the array in ascending order2', () => {
	const arr = [35,42.13,13,72.94,8,14.6,9.1];
	const arrSorted = [8,9.1,13,14.6,35,42.13,72.94];
	const newArr = sorting(arr);
	expect(newArr).to.eql(arrSorted);
})

let sensors = [
	{
		'SiteName': 'A',
		'PM2.5': "30"
	},
	{
		'SiteName': 'B',
		'PM2.5': "20"
	},
	{
		'SiteName': 'C',
		'PM2.5': "10"
	},
	{
		'SiteName': 'D',
		'PM2.5': "13.5"
	},
	{
		'SiteName': 'E',
		'PM2.5': "10"
	},
	{
		'SiteName': 'Egg',
		'PM2.5': "10"
	},
	{
		'SiteName': 'CcC',
		'PM2.5': "10"
	},
	{
		'SiteName': 'BooBoo',
		'PM2.5': "10"
	},
];

test('Should sort the sensors by name in ascending order', () => {
	let sortedSensors = sensors.sort(compare);
	expect(sortedSensors).to.be.sortedBy("PM2.5", {descending: false})
})


test('Should return average of an array', () => {
	const avg = average([1,2,3]);
	expect(avg).to.equal(2);
})


test('Should round to 2 decimal places', () => {
	const avg = average([12.546, 24.3235, 15.7345]);
	expect(avg).to.equal(17.53);
})
