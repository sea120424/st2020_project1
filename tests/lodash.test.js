const _ = require('lodash');
const chai = require("chai");
const expect = chai.expect; 


test('Should reverse an array', () => {
    const array = [1, 2, 3];
    let reversedArr = _.reverse(array);
    expect(reversedArr).to.eql([3, 2, 1]);
})

test('Should reverse a float array', () => {
    const array = [4.5, 1.6, 3.14159, 22000, 88];
    let reversedArr = _.reverse(array);
    expect(reversedArr).to.eql([88, 22000, 3.14159, 1.6, 4.5]);
}) //1

test('Should divided into correct chunks', () => {
    const array = ['a', 'b', 'c', 'd', 'dog', 'cat'];
    let chunk_size = 3
    let reversedArr = _.chunk(array, chunk_size);
    expect(reversedArr).to.eql([['a', 'b', 'c'], ['d', 'dog', 'cat']]);
}) //2


test('Should output the different value', () => {
    const array_a = [3, 6, 9 ,12, 15];
    const array_b = [3, 5, 9 ,11, 15];
    let array_diff = _.difference(array_a, array_b);
    expect(array_diff).to.eql([6, 12]);
}) //3


test('Should drop the corret number of elements', () => {
    const array = [3, 6, 9 ,12, 15];
    const num = 3;
    let dropped_array = _.drop(array, num);
    expect(dropped_array).to.eql([12, 15]);
}) //4


test('Should drop the corret number of elements from right', () => {
    const array = [3, 6, 9 ,12, 15];
    const num = 3;
    let dropped_array = _.dropRight(array, num);
    expect(dropped_array).to.eql([3, 6]);
}) //5

test('Should correctly fill the target element', () => {
    const array = [3, 6, 9];
    const value = '*';
    let filled_array = _.fill(array, value);
    expect(filled_array).to.eql(['*', '*', '*']);
}) //6


test('Should correctly flatten the whole array', () => {
    const array = [[1, [2, [3, [4]], 5]]];
    let flatten_array = _.flattenDeep(array);
    expect(flatten_array).to.eql([1, 2, 3, 4, 5]);
}) //7


test('Should return the first element of the array', () => {
    const array = [1, [2, [3, [4]], 5]];
    let head = _.head(array);
    expect(head).to.eql(1);
}) //8


test('Should return the intersection of two array', () => {
    const array_a = [1, 3, 5, 7, 9];
    const array_b = [1, 4, 5, 8, 10];
    let intersection = _.intersection(array_a, array_b);
    expect(intersection).to.eql([1, 5]);
}) //9

test('Should combine the elements of array with a sepaeator', () => {
    const array = [1, 3, 5, 7, 9];
    const sep = '-';
    let string = _.join(array, sep);
    expect(string).to.eql('1-3-5-7-9');
}) //10


