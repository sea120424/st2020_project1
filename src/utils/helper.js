
let sorting = (array) => {
    let sorted_arr = array.sort(function(a, b){
  	return a - b;
    });
    return array;
}

let compare = (a, b) => {
	let a_float = parseFloat(a['PM2.5']);
	let b_float = parseFloat(b['PM2.5']);
	if(a_float > b_float){
		return 1;
	}
	else if(a_float === b_float){
		if(a['SiteName'] < b['SiteName']){
			return 1;
		}
		else{
			return -1;
		}
	}
	else{
		return -1;
	}
}

let average = (nums) => {	   
    let sum = nums.reduce((previous, current) => current += previous);
    let avg = Math.round((sum / nums.length) * 100) / 100;
    return avg;
}


module.exports = {
    sorting,
    compare,
    average
}
